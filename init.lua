minetest.register_craft({
	output = 'default:mese_crystal',
	recipe = {
		{"default:mese_crystal_fragment", "default:mese_crystal_fragment", "default:mese_crystal_fragment"},
		{"default:mese_crystal_fragment", "default:mese_crystal_fragment", "default:mese_crystal_fragment"},
		{"default:mese_crystal_fragment", "default:mese_crystal_fragment", "default:mese_crystal_fragment"},
	},
})
minetest.register_craft({
	output = 'default:mese 54',
	recipe = {
		{"", "", ""},
		{"", "default:nyancat", ""},
		{"", "", ""},
	},
})
minetest.register_craft({
	output = 'default:mese 9',
	recipe = {
		{"", "", ""},
		{"", "default:nyancat_rainbow", ""},
		{"", "", ""},
	},
})
minetest.register_craft({
	output = 'default:nyancat_rainbow',
	recipe = {
		{"default:mese", "default:mese", "default:mese"},
		{"default:mese", "default:mese", "default:mese"},
		{"default:mese", "default:mese", "default:mese"},
	},
})
minetest.register_craft({
	output = 'default:nyancat',
	recipe = {
		{"default:nyancat_rainbow", "default:nyancat_rainbow", "default:nyancat_rainbow"},
		{"default:nyancat_rainbow", "default:nyancat_rainbow", "default:nyancat_rainbow"},
		{"", "", ""},
	},
})
